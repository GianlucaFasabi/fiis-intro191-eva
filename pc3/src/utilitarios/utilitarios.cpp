/*
 * utilitarios.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
using namespace std;

void imprimir(int A[], int n){
	cout << "[ ";
	for(int i = 0; i < n; i++){
		cout << A[i] << " ";
	}
	cout << " ]" << endl;
}

void imprimir(string nombreVector, float *A, int tamano){
	cout << nombreVector << " = [ ";
	for(int i = 0; i < tamano; i++){
		cout << A[i] << " ";
	}
	cout << "] " << endl;

}

void imprimir(string nombreVector, int *A, int tamano){
	cout << nombreVector << " = [ ";
	for(int i = 0; i < tamano; i++){
		cout << A[i] << " ";
	}
	cout << "] " << endl;

}






