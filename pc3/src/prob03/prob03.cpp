/*
 * prob03.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;


void prob03(){
	int n = 0;
	int suma= 0;
	//int promedio = 0;
	cout << "Ingrese el orden de la matriz: ";
	cin >> n;
	while((n%2)==0){
		cout << "Ingrese el orden de la matriz: ";
		cin >> n;
	}
	//Ingresan los elementos de la matriz
	int matriz[n][n];
	for(int i = 0; i < n; i++){
		for(int j = 0; j<n; j++){
			cout << "Ingrese elemento [" << i+1 <<"][" << j+1 <<"]: ";
			cin >> matriz[i][j];
		}
	}
    //Volver en ceros los elementos que no queremos para luego hacer un contador de elementos que no sean ceros, hallar
	//la suma de todos los elementos de la metriz y dividirla entre el contador
	for(int k=0;k<n;k++){
		for(int l= 0; l<n;l++){
			if(k<=(n+1)*0.5){
				matriz[k][l] = 0;
			}
			if(l+n==n-1){
				matriz[k][l] = 0;
			}
			if(l==0||l==n-1){
				matriz[k][l] = 0;
			}
			if(k==l){
				matriz[k][l] = 0;
			}
		}
	}
	for(int m = 0; m < n; m++){
		for(int o = 0; o<n; o++){
			suma+=matriz[m][o];
		}
	}
}

