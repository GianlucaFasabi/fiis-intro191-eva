/*
 * prob02.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

int sumaMatriz(int a[][2], int b[][2], int filas, int columnas){
	int contador = 0;
	for(int i = 0; i < filas; i++){
		cout << "|";
		for(int j = 0; j < columnas; j++){

				if((a[i][j] + b[i][j])>255){
				contador++;
				int m = a[i][j] + b[i][j];
				m = 255;
				cout << m;
		}
				else{
					cout << a[i][j] + b[i][j];
				}
			if(j<columnas-1){
				cout << " ";
			}
			if(j==columnas-1){
				cout << "|" << endl;
			}
		}
	}
	if(contador==filas*columnas){
		cout << "Es completamente blanco";
	}
	return 0;
}

void prob02(){
	int a[2][2] = {210,215,220,214};
	int b[2][2] = {210,215,220,214};
	sumaMatriz(a,b,2,2);
}

