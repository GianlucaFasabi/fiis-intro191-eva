/*
 * prob01.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

int recursiva(int a, int b) {
	if (a < b) {
		return recursiva(b - a, a);
	}
	if (b < a) {
		return recursiva(a - b, b);
	}
	if (b == a) {
		return a;
	}
}
void prob01() {
	int m = recursiva(133, 31);
	cout << m;
}

