/*
 * prob03.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

//Hacemos una funcion que determine si es primo o no
//void primo(int n){
	//for(int m = 2; m < n/2; m++){
	//}
//}
void prob03(){
	int n;
	int contador = 0;
	float suma = 0;
	cout << "Ingrese orden de la matriz: ";
	cin>>n;
	while(n%2==0||n>100){
	cout << "Ingrese orden de la matriz: ";
	cin >> n;
	}
	int matriz[n][n];
	//Rellenamos la matriz
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			cout << "Ingrese elemento [" << i+1 << "][" << j+1 << "]: ";
			cin >> matriz[i][j];
		}
	}
	//Hacemos un criterio para que el solo los elementos primos de por encima y por debajo de las diagonales aumenten el contador y la suma
	for(int k = 0; k < n; k++){
		for(int l = 0; l < n; l++){
			//Con este if restringimos los elementos que estan por encima y por debajo de las diagonales
			if((l>k&&l+k<n-1)||(l<k&&k+l>n-1)){
				//Debemos restringir que son numeros primos dentro de este bucle para aumentar la suma y el contador
				//Aqui llamariamos a la funcion que determine si es primo o no
				suma+=matriz[k][l];
				contador++;
				cout << matriz[k][l] << " ";
			}
		}
	}
	//Falta restringir que sean primos y lo que imprimiriamos en pantalla es la division entre la suma y el contador
	cout << suma/contador;
}
