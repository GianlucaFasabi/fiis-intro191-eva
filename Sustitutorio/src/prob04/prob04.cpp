/*
 * prob04.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

void prob04(){
	int n;
	cout << "Ingrese el peso total del bloque: ";
	cin >> n;
	while(!(n%2==0)){
	cout << "Ingrese el peso total del bloque: ";
	cin >> n;
	}
	int a = n%5;
	int b = (n-5*a)%3;
	int c = (n-3*b)%2;
	cout << "Hay " << n/5 -2 << " bloques de 5" <<endl;
	cout << "Hay " << b+1 << " bloques de 3" <<endl;
	cout << "Hay " << c+1 << " bloques de 2" <<endl;
	cout << "El peso total de los cortes es: " << (n/5 -2)*5 + (b+1)*3 + (c+1)*2;
}

